import React from 'react';
import {
  InterfaceTodo,
  InterfaceTodoListComponent,
} from '../interface/Interface_Ex_TodoList';
import TodoItem from '../TodoItem/TodoItem';

export default function TodoList({ todos, handleTodoRemove, handleTodoComplete }: InterfaceTodoListComponent) {
  console.log('todos: ', todos);
  return (
    <div className='container mx-auto'>
      <div className='overflow-x-auto relative'>
        <table className='w-full text-sm text-left text-gray-500 dark:text-gray-400'>
          <thead className='text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400'>
            <tr>
              <th scope='col' className='py-3 px-6'>
                ID
              </th>
              <th scope='col' className='py-3 px-6'>
                name
              </th>
           
              <th scope='col' className='py-3 px-6'>
                isComplete
              </th>
              <th></th>
            </tr>
          </thead>
          <tbody>
           {todos.map((item) => { 
            return <TodoItem key={item.id} todo={item} handleTodoRemove={handleTodoRemove} handleTodoComplete={handleTodoComplete}/>
            })}
           
          </tbody>
        </table>
      </div>
    </div>
  );
}
