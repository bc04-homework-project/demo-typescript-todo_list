import React, { useState } from 'react';
import { InterfaceTodo } from './interface/Interface_Ex_TodoList';
import TodoForm from './TodoForm/TodoForm';
import TodoList from './TodoList/TodoList';

export default function Ex_TodoList() {
  const [todos, setTodos] = useState<InterfaceTodo[]>([
    {
      id: '1',
      text: 'lam du an cuoi khoa',
      isComplete: false,
    },
    {
      id: '2',
      text: 'lam capstone project',
      isComplete: false,
    },
  ]);

  const handleTodoAdd = (todo: InterfaceTodo) => { 
    let newTodos = [...todos, todo];
    setTodos(newTodos)
   }

  const handleTodoRemove = (idTodo:string) => { 
    let newTodos = todos.filter((todo) => todo.id !== idTodo)
    setTodos(newTodos)
   }

   const handleTodoComplete = (idTodo:string) => { 
    let index = todos.findIndex((todo) => todo.id == idTodo);
    // todos[index].isComplete = true;
    todos[index].isComplete = !todos[index].isComplete;
    let newTodos = [...todos];
    setTodos(newTodos)
     
    }
  return (
    <div>
      <TodoForm handleTodoAdd={handleTodoAdd} />
      <TodoList todos={todos} handleTodoRemove={handleTodoRemove} handleTodoComplete={handleTodoComplete}/>
    </div>
  );
};
