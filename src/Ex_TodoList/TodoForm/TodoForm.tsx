import { nanoid } from 'nanoid';
import React, { useState } from 'react';
import {
  InterfaceTodo,
  InterfaceTodoFormComponent,
} from '../interface/Interface_Ex_TodoList';

export default function TodoForm({
  handleTodoAdd,
}: InterfaceTodoFormComponent) {
  const [title, setTitle] = useState<string>('');

  const handleOnChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    let value = e.target.value;
    setTitle(value);
  };

  const handleTodoCreate = () => {
    let newTodo: InterfaceTodo = {
      id: nanoid(),
      text: title,
      isComplete: false,
    };
    handleTodoAdd(newTodo);

    setTitle('');
  };

  return (
    <div className='my-20 flex container mx-auto'>
      <input
        onChange={handleOnChange}
        type='text'
        className='p-5 border border-blue-300 flex-grow'
        value={title}
      />
      <button
        onClick={handleTodoCreate}
        className='bg-red-300 text-white rounded px-5 py-2 flex-shrink-0 hover:bg-blue-400'
      >
        Add Todo
      </button>

      
    </div>
  );
}
