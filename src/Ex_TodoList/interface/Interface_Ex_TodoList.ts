export interface InterfaceTodo {
  id: string;
  text: string;
  isComplete: boolean;
}

export interface InterfaceTodoListComponent {
  todos: InterfaceTodo[];
  handleTodoRemove: (idTodo:string) => void;
  handleTodoComplete:(idTodo:string) => void;
}

export interface InterfaceTodoItemComponent {
  todo: InterfaceTodo;
  handleTodoRemove: (idTodo:string) => void;
  handleTodoComplete:(idTodo:string) => void;
}

export interface InterfaceTodoFormComponent {
  handleTodoAdd: (todo: InterfaceTodo) => void;
}
