import React from 'react';
import { InterfaceDataUser } from '../DemoProps';

interface InterfaceUserInfor{
user: InterfaceDataUser
}

export default function UserInfor({user}:InterfaceUserInfor) {
  return <div><p>UserInfor</p>
  <p>{user.name}</p>
  <p>{user.age}</p>
  </div>;
}


// export default function UserInfor(data2:InterfaceUserInfor) {
//   return <div><p>UserInfor</p>
//   <p>{data2.user.name}</p>
//   <p>{data2.user.age}</p>
//   </div>;
// }


